h1. Build steps

h2. Build Apache Feliex Utils 

At first the org.apache.felix.util bundle needs to be built. So you have to execute 

@mvn clean install@ in the @plugins/org.eclipselabs.osgi.felix.utils@.

h2. Build the features and update site

Execute @mvn clean verify@ in the parent folder.
